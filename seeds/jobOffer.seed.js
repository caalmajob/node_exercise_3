const mongoose = require('mongoose');
const db = require('../db');
const JobOffer = require('../models/JobOffer');

const jobOffer = [
    {
        title: "Front End Developer",
        description: "Desde Avansis estamos ampliando nuestro equipo en el sector bancario con un Programador Frontend Senior.",
        company: "Avansis",
        contactEmail: "joboffer@Avansis.com",
        contactId: "6064de4c3bca361917a9ec8e",
},
{
    title: "Front End Developer",
    description: "En TopBarcos.com / TopBoats.com, red de 11 portales y líder internacional desde 2005 , vamos a incorporar 2 nuevos Desarroladores Front-End a nuestro equipo de desarrollo.",
    company: "TopBarcos",
    contactEmail: "joboffer@Topbarcos.com",
    contactId: "6064de4c3bca361917a9ec8e",
},
{
    title: "Front End Developer",
    description: "Buscamos desarrolladores Front End para integrarse en nuestro equipo de Software Factory. Requerimos un buen conocimiento de desarrollo front-end basado en HTML/CSS/JavaScript especializado en Angular, VUE js, React, etc..., preferiblemente con base de programación orientada a objetos y conceptos de diseño y arquitectura de sistemas de software",
    company: "SDG Consulting España",
    contactEmail: "joboffer@sdg.com",
    contactId: "6064de4c3bca361917a9ec8e",
},
{
    title: "Repartidor",
    description: "Buscamos repartidor de comida a domicilio en Madrid capital, con horario flexible",
    company: "Pizza Hut",
    contactEmail: "joboffer@pizzahut.com",
    contactId: "6064de4c3bca361917a9ec8e",
},
{
    title: "camarero",
    description: "Buco camarero para bar en el centro de Madrid, con dos años de experiencia",
    company: "Bar el Camino",
    contactEmail: "joboffer@elcamino.com",
    contactId: "6064de4c3bca361917a9ec8e",
},
];

mongoose
    .connect(db.DB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(async () => {
    console.log('Deleting all offers...');
    const allOffers = await JobOffer.find();
    

    if(allOffers.length) {
        await JobOffer.collection.drop();
    }
    })
    .catch(error => {
        console.log('Error deleting data: ', error)
    })
    .then( async() => {
        await JobOffer.insertMany(jobOffer);
        console.log('Successfully added users to DB...');
    })
    .finally(() => mongoose.disconnect());

