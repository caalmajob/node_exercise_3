const express = require('express');
const passport = require('passport');
const path = require('path');
const session = require('express-session');
const MongoStore = require('connect-mongo');
const db = require('./db.js');
const indexRoutes = require('./routes/ index.routes');
const authRoutes = require('./routes/auth.routes');
const JobOfferRoutes = require('./routes/jobOffer.routes');

require('./passport/passport');

db.connect();

const PORT = 3000;
const app = express();

app.use(session({
    secret: 'AasQsfi.123-@',
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 48 * 60 * 60 * 1000
    },
    store: MongoStore.create({ mongoUrl: db.DB_URL}),
}));

app.set('viewns', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(express.static(path.join(__dirname, 'public')));

app.use(passport.initialize());
app.use(passport.session());

app.use(express.json());
app.use(express.urlencoded({ extended: true}));

app.use('/', indexRoutes);
app.use('/auth', authRoutes);
app.use('/jobs', JobOfferRoutes);

app.use('*', (req, res, next) => {
    const error = new Error('Route not found');
    error.status = 404;
    next(error);
})

app.use((error, req, res, next) => {
    return res.status(error.status || 500).render('error', {error: error.message || 'Unexpected error'});
});

app.listen(PORT, () => {
    console.log(`Server listening in http://localhost:${PORT}`)
});
