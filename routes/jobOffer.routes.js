const express = require('express');
const { isAuthenticated } = require('../middlewares/auth.middleware');
const JobOffer = require('../models/JobOffer');
const router = express.Router();
const { upload } = require('../middlewares/files.middleware');


router.get('/offers',[isAuthenticated], async (req, res, next) => {
    try {
    const jobOffer = await JobOffer.find();
    return res.render('jobOffer', { jobOffer, user: req.user } );
    } catch(error) {
        next(new Error(error));
    }
});

router.get('/search', async (req, res, next) => {
    try {
    const { title } = req.query;
    if(!title) {
        const error = new Error('No se encuentra ninguna coincidencia')
        return res.render('search', {user: req.user, error: error.message});
    }
    const jobs = await JobOffer.find({title});
    return res.render('search',  { jobs });
    } catch(error) {
        next(new Error(error));
    }
});

router.get('/create', [isAuthenticated], (req, res) => {
    return res.render('create', {user: req.user});
});


router.post('/create', upload.single('avatar'), async (req, res, next) => {
    try {
    const { title, description, company, contactEmail, contactId } = req.body;

    if(!title || !description ||!company || !contactEmail) {
        const error = new Error('Completa todos los campos')
        return res.render('create', {user: req.user, error: error.message});
    }

    let image;

        if(req.file) {
            image = req.file.filename;
        };
        const newOffer = await new JobOffer({ title, description, company, contactEmail, contactId, image });

        const createdStore = await newOffer.save();

        return res.redirect('/jobs/offers');

    } catch (error) {
        next(error);
    }
});



module.exports = router;
