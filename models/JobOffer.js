const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const JobOfferSchema = new Schema({
    title: { type: String, required: true },
    description: { type: String, required: true },
    company: { type: String, required: true },
    contactEmail: { type: String, required: true },
    image: {type: String },
    contactId: { type: mongoose.Types.ObjectId, ref: 'User'},
}, {
    timestamps: true,
});

const JobOffer = mongoose.model('JobOffer', JobOfferSchema);

module.exports = JobOffer;